package filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class filterCreatorGui {
	
	private final Scanner in = new Scanner(System.in);
	
	public filterCreatorGui() {}
	
	public Filter getUserFilter() {
		String origin = getOrigin();
		String dest = getDestination();
		return getOptinalData(getFilterType());
	}
	
	private String getOrigin() {
		//Get origin using command line. Maybe add destination/ origin spell checking
		System.out.println("Enter in Orgin");
		return in.nextLine();
	}
	
	private String getDestination() {
		//Get destination using command line
		System.out.println("Enter in Destination");
		return in.nextLine();
	}
	
	private filterType getFilterType() {
		List<filterType> filters = Arrays.asList(filterType.values());
		System.out.println("Enter in filter Type form list below. Use number");
				
		boolean valid = false;
		while(!valid) {
			for(int i = 0; i < filters.size(); i++) {
				System.out.println(i + ". " + filters.get(i));
			}
			int filter = in.nextInt();
			if(filter < filters.size())
				valid = true;
			else
				System.out.println("Choose a correct number!");
		}
		return null;
	}
	
	private Date getStartDate() {
		//Get start date using command line
		System.out.println("Enter in start date");
		return null;
	}
	
	private Date getEndDate() {
		//Get end date using command line
		System.out.println("Enter in end date");
		return null;
	}
	
	private Filter getOptinalData(filterType filter) {
		switch(filter.ordinal()) {
			case 0 : {};
			case 1 : {};
			case 2 : {};
		}
		return null;
	}
}
