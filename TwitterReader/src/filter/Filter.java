package filter;

import main.Flight;

public interface Filter {
	public boolean match(Flight flight);
}
