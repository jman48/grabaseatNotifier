package main;

import java.util.ArrayList;
import java.util.List;

import feed.Feed;
import filter.Filter;

public final class User {
	private final String name;
	private final String email;
	private final List<Flight> filters;
	private static List<User> users = new ArrayList<User>();
	
	private User(Builder builder) {
		this.name = builder.name;
		this.email = builder.email;
		this.filters = builder.filters;
	}
	
	public List<Flight> getFilters() {
		return new ArrayList<Flight>(filters);
	}
	
	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}
    
    public void notify(Flight deal) {
        Email.alert(this.email, deal);
    }

	public static class Builder {
		private String name;
		private String email;
		private List<Feed> feeds;
		private List<Flight> filters;
		
		public Builder() {}
		
		public Builder setName(String name) {
			this.name = name;
			return this;
		}
		
		public Builder setEmail(String email) {
			this.email = email;
			return this;
		}
		
		public Builder setFeed(List<Feed> feeds) {
			this.feeds = new ArrayList<Feed>(feeds);
			return this;
		}
		
		public Builder setFilters(List<Flight> filters) {
			this.filters = new ArrayList<Flight>(filters);
			return this;
		}
		
		/**
		 * TODO : Add validity checks on args
		 * @return
		 */
		public User build() {
			/*
			if(Builder.name == null || Builder.email == null || Builder.feeds == null) {
				throw new NullPointerException("Arguments are not allowed to be null");
			}
			*/
			return new User(this);
		}
	}
	
	/*
	 * TODO: Access a database and get all users
	 */
	public static List<User> getUsers() {
		return users;		
	}
	
	public static boolean saveUser(User user) {
		return users.add(user);
	}
}
