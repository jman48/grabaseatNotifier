package main;
import java.util.Date;

/**
 * A class that represents a flight. 
 * Build using a functional design.
 * 
 * 
 * @author John Armstrong <j.armstrong484@gmail.com> *
 */
public final class Flight {
	
    public final String EMPTYPORT = "EMPTYPORT";
    
	private final String origin;
	private final String dest;
	
	//Price in cents
	private final int price;
	private final Date date;
	
    public Flight(Flight flight) {
        this.origin = flight.origin;
        this.dest = flight.dest;
        this.price = flight.price;
        this.date = new Date(flight.date.getTime());
    }
    
    /*
     * Should change to use a builder
     * Should use actual check not assert
     */
	public Flight(String origin, String dest, int price, Date date) {
		assert(price > 0);
		checkArgs(origin, dest);
		
		this.origin = origin;
		this.dest = dest;
		this.price = price;
		this.date = new Date(date.getTime());
	}
	
	public String getOrigin() {
		return origin;
	}

	public String getDest() {
		return dest;
	}

	public int getPrice() {
		return price;
	}

	public Date getDate() {
		return new Date(date.getTime());
	}
	
	public String toString() {
		return "Origin : " + this.origin + "\nDest : " + this.dest  + "\nPrice : $" + this.price
				+ "\nStart Date : " + this.date.toString();
	}
	
	private void checkArgs(String origin, String dest) {
		if(origin == null || dest == null ) {
			throw new IllegalArgumentException("Args must not be null");
		}
	}
}
