package main;

import io.Input;
import io.InputImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import feed.Feed;
import feed.FeedFactory;
import feed.FeedType;
import filter.filterCreator;
import filter.filterCreatorGui;


public class Main {
	public static void main(String[] args){
		
		getUserInput();
        
        List<User> users = User.getUsers();
            
        List<Flight> flights = getFlights();
        
        for(User user : users) {
        	for(Flight query : user.getFilters()) {
        		new Query(flights, query, user).run();
        	}
        }
	}
	
	private static List<Flight> getFlights() {
		Feed feed = FeedFactory.getInstance(FeedType.TWITTERGRABASEAT);      
        return feed.retrieveFlights();
	}
	
	private static boolean getUserInput() {
		Scanner in = new Scanner(System.in);
		Input userIn = new InputImpl();
		
		print("Select an option");
		print("1 - New user");
		
		int opt = in.nextInt();
		
		switch(opt) {
		case 1:
			return userIn.addUser();
		default:
			return false;
		}
	}
	
	private static void print(String msg) {
		System.out.println(msg);
	}
}
