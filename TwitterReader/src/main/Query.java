package main;

import java.util.ArrayList;
import java.util.List;

import feed.Feed;
import feed.FeedType;
import filter.Filter;

/**
 * A class to compare flights to user set filters.
 * 
 * **Design Comments**
 * Used a final class as not designed for inheritance. 
 * 
 * 
 * @author John Armstrong <j.armstrong484@gmail.com>
 *
 */
public final class Query implements Runnable {
	private final User user;
	private final List<Flight> flights;
	private final Flight deal;
	
	public Query(List<Flight> flights, Flight deal, User user) {
        this.flights = new ArrayList<Flight>(flights);
        this.deal = new Flight(deal);
        this.user = user;
	}
    
    public Query setFlights(List<Flight> flights) {
        return new Query(flights, deal, user);
    }
	
	
	@Override
	public void run() {
        for(Flight flight : flights) {
            if(compare(flight, deal)) {
                user.notify(flight);
            }		
    	}
	}
    
    public boolean compare(Flight flight, Flight deal) {
        return checkPort(flight, deal) && checkPrice(flight, deal);
    }
    
    private boolean checkPort(Flight flight, Flight deal) {
        if(flight.getOrigin() == null || flight.getDest() == null) {
            return false;
        }
        else {
            return flight.getOrigin().equals(deal.getOrigin()) 
                && flight.getDest().equals(deal.getDest());
        }
    }
    
    private boolean checkPrice(Flight flight, Flight deal) {
        if(deal.getPrice() == 0) {
            return true;	//return true because might want to find cheap flight without worrying about price.
        }
        else {
            return flight.getPrice() <= deal.getPrice();
        }
    }
    
    private boolean compareDates(Flight flight, Flight deal) {
        if(flight.getDate() == null) {
            return true;
        }
        else {
        	return flight.getDate().equals(deal.getDate());
        }
    }
}