package feed;

public class FeedException extends Exception {
	public FeedException(String message) {
		super(message);
	}
}
