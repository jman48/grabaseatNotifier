package feed;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public final class TwitterRetriever implements FeedRetriever{
	
	private final String consumerKey = "ozLlAU9nhiKSEefAPgfxQ";
	private final String consumerSecret = "5QyZkPWC4ZShMPWx3I5EhKweqv72B0K3Hgq5CPbr37Y";
	private final String accessToken = "2218989968-sEgQFD6hpJHaYkD3gSpEa9HngfEJmdpIszTlEYu";
	private final String accessTokenSecret = "ww5wnBXMvs4N2ZPXhet8LuvhwQDvfuyttn0EycQIxUDUa";
	private final Twitter twitter;
	
	public TwitterRetriever() {
		//Configuration
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(consumerKey)
		  .setOAuthConsumerSecret(consumerSecret)
		  .setOAuthAccessToken(accessToken)
		  .setOAuthAccessTokenSecret(accessTokenSecret);
		
		//Build instance
		TwitterFactory tf = new TwitterFactory(cb.build());
		twitter = tf.getInstance();
	}
	
	/*
	public List<String> retrive() throws TwitterException {		
			
	    List<Status> statuses = twitter.getUserTimeline("@grabaseat");
	    List<String> tweets = new ArrayList<String>();
	    for (Status status : statuses) {
	        tweets.add(status.getText().toLowerCase());
	    }
	    return new GrabASeatBuilder().filter(tweets);
	}
	*/

	@Override
	public List<String> retriveFeed() throws FeedException {
		List<String> flights = new ArrayList<String>();
		try{
			List<Status> statuses = twitter.getUserTimeline("@grabaseat");
			for (Status status : statuses) {
			    flights.add(status.getText().toLowerCase());
			}
		}
		catch(TwitterException twitExp) {
			throw new FeedException("There was a general error in retrieving the feed. Try again later");
		}
		return flights;
	}	
}