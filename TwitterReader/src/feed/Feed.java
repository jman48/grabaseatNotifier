package feed;

import java.util.List;

import main.Flight;

public interface Feed {
	
	/**
	 * Returns a list of flights
	 * @return
	 */
	public List<Flight> retrieveFlights();
}
