package feed;

import java.util.List;

public interface FeedRetriever {
	public List<String> retriveFeed() throws FeedException;
}
