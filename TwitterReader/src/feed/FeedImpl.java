package feed;

import java.text.ParseException;
import java.util.List;

import parser.FlightFeedParser;
import main.Flight;

public class FeedImpl implements Feed{
	private final FlightFeedParser parser;
	private final FeedRetriever retriever;
	
	/**
	 * TODO : Defensive copy args
	 * 
	 * @param retriever
	 * @param parser
	 */
	public FeedImpl(FeedRetriever retriever, FlightFeedParser parser) {
		this.parser = parser;
		this.retriever = retriever;
	}
	
	
	@Override
	public List<Flight> retrieveFlights() {
		try {
			return parser.parse(retriever.retriveFeed());
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (FeedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
