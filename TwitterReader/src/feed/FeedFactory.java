package feed;

import parser.FlightFeedParser;
import parser.GrabASeatParser;

/*
 * Use a feed factory as one day may implement it for other sites/ feeds
 */
public class FeedFactory {
		
	public static Feed getInstance(FeedType type) {
		switch(type){
			case TWITTERGRABASEAT: {
				return new FeedImpl(new TwitterRetriever(), new GrabASeatParser());			
			}	
		}
		return null;
	}
}
