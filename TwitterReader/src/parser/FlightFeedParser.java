package parser;

import java.text.ParseException;
import java.util.List;

import main.Flight;

public interface FlightFeedParser {
	public List<Flight> parse(List<String> items) throws ParseException;
}