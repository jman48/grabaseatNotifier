package parser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import main.Flight;

public final class GrabASeatParser implements FlightFeedParser {

	private final DateFormat d = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
	
	@Override
	public List<Flight> parse(List<String> items) throws ParseException {
		// TODO Auto-generated method stub
		List<String> tweets = new ArrayList<String>(items);
		List<Flight> flights = new ArrayList<Flight>();
		for(String tweet : tweets) {	
			if(validateTweet(tweet)) {
				Flight flight = new Flight(getOrigin(tweet), getDest(tweet), getPrice(tweet), getDate(tweet));
				flights.add(flight);
			}
		}
		return flights;
	}
	
	private String getOrigin(String tweet) {
		int to = tweet.indexOf(" to ");
		return tweet.substring(0, to);
	}
	
	private String getDest(String tweet) {
		int from = tweet.indexOf(" to ")+4;
		int to = tweet.indexOf("one way")-1;
		return tweet.substring(from, to);
	}
	
	private int getPrice(String tweet) {
		int from = tweet.indexOf("$")+1;
		int to = tweet.indexOf(";")-3;
		return Integer.parseInt(tweet.substring(from, to));
	}
	
	private Date getDate(String tweet) throws ParseException {
		int from = tweet.indexOf(";")+2;
		int to = tweet.indexOf(" - ", from);
		String sub = tweet.substring(from, to);
		//TODO: Change to use dynamic year
		return d.parse(getMonth(sub) + " " + getDay(sub) + " 2014");
	}
	
	private String getDay(String text) {
		return text.substring(0, 2);		
	}
	
	private String getMonth(String text) {
		return text.substring(3);
	}
	
	private boolean validateTweet(String tweet) {
		if(tweet.contains("@") || !tweet.contains("seat count"))
			return false;
		return true;
	}

}
