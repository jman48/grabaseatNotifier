package io;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import main.Flight;
import main.User;

/**
 * A class to get user input and construct objects
 * 
 * @author John
 *
 */
public class InputImpl implements Input {

	/**
	 * Get input from the user and then construct and save a User object
	 */
	@Override
	public boolean addUser() {
		String userName = "";
		String userEmail = "";
		
		boolean complete = false;
		Scanner in = new Scanner(System.in);
		
		while(!complete) {
			print("Enter in new user name");
			userName = in.nextLine();
			
			print("Enter in new user email address");
			userEmail = in.nextLine();
			
			if(userName.length() > 0 && userEmail.length() > 0 && userEmail.contains("@")) {
				complete = true;
			}
			else {
				print("There was an error with your input. Please try again");
			}
		}
		User user = null;
				
		Flight filter = getFilter();
		List<Flight> filters = new ArrayList<Flight>();
		filters.add(filter);
		
		user = new User.Builder().setName(userName).setEmail(userEmail).setFilters(filters).build();
	
		in.close();
		
		return User.saveUser(user);
	}

	/**
	 * Get user input and delete that User object
	 */
	@Override
	public boolean deleteUser() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Get user input and construct a flight filter for a User
	 */
	private Flight getFilter() {
		String origin = null;
		String dest = null;
		Date date = null;
		int price = 0;
		
		boolean complete = false;
		Scanner in = new Scanner(System.in);
		String errMsg = "";
		
		while(!complete) {
			print(errMsg);
			errMsg = "";
			
			print("Enter in filter origin");
			origin = in.nextLine();
			
			print("Enter in filter destination");
			dest = in.nextLine();
			
			print("Enter in filter date (Format: MMMM d, yyyy");
			try {
				date = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).parse(in.nextLine());
			} catch (ParseException e) {
				errMsg = e.getMessage();
				continue;
			}
			
			print("Enter in filter price");
			price = in.nextInt();
			
			if(!correctInputs(origin, dest)) {
				errMsg = "Check origin or destination";
			}
			else {
				complete = true;
			}
		}		
		return new Flight(origin, dest, price, date);
	}
	
	private void print(String msg) {
		System.out.println(msg);
	}
	
	private boolean correctInputs(String origin, String dest) {
		return (origin.length() > 0 || origin != null) || 
				(dest.length() > 0 || dest != null);
	}

}
