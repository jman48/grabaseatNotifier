package io;

import main.Flight;
import main.User;

public interface Input {

	public boolean addUser();
	
	public boolean deleteUser();
}
