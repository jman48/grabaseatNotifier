<h2>Flight deal notifier</h2>

<p>This is a resurrection of an old project of mine. It is a flight notifier. 
It grabs all the flights off grabaseat.com and checks it against some preset filters such as price, origin, destination and date. If it finds a match it will email the user that set that filter.
Great for finding some cheap flights! </p>

<p>Currently begin re coded in rails to run on server</p>

<p>May have a bit of a funny looking structure as this project was meant to be a practice at applying design patterns. Therefore it will not be made as efficently as possible but contains lots of examples of design patterns.</p>

<p>Still a work in progress</p>
